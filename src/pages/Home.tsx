import { IonContent, 
        IonHeader, 
        IonPage, 
        IonTitle, 
        IonToolbar, 
        IonCard, 
        IonCardHeader, 
        IonCardTitle, 
        IonCardContent,
        IonCardSubtitle, 
        IonImg,
        IonGrid, 
        IonCol, 
        IonRow, 
        IonSearchbar,
        IonButton,
        IonItemDivider,
        // IonInput
} from '@ionic/react';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';


class Home extends Component{

  state = {
    isLoading: false,
    movies:  []
  }


fetchmovies(){
  fetch('https://api.themoviedb.org/3/movie/popular?api_key=298d6561dc4ad9c82d228128330b843d&language=en-US&page=10')
    .then(response => response.json())
    .then(data =>
      this.setState({
        movies: data.results,
        isLoading: true
      })
       )
}

componentDidMount(){
  this.fetchmovies();
 }
   



  render(){  

const {isLoading, movies } = this.state;
  
  return (
    <IonPage>
      <IonHeader>
      <IonToolbar color="dark">
        <IonTitle>MovieDB</IonTitle>
    </IonToolbar>
      </IonHeader>
      <IonContent className="ion-padding">
        <IonGrid>
            <IonRow>
                <IonCol size="12">
                      <IonSearchbar showCancelButton="always" placeholder="search a movie" id="movie_name"></IonSearchbar>
                </IonCol>
            </IonRow>
            <IonCol size-lg= "6">
            {isLoading !== false ? 
          ( movies.map(movie => {
            const { title, overview, poster_path, release_date, id } = movie;
            return (
             
                <IonCard size-md="6">
                  <IonCardHeader>
                  <IonImg src={'https://image.tmdb.org/t/p/w500/' + poster_path} alt="image"/>
                    <IonCardTitle>
                        {title}
                    </IonCardTitle>
                    <IonCardSubtitle>Released Date: {release_date}</IonCardSubtitle>
                    <IonCardContent>
                      {overview}
                    </IonCardContent>
                      <IonItemDivider></IonItemDivider>
                     <Link to = {`/Show/${id}`}>
                        <IonButton fill="outline" color="dark" expand="block" slot="end" shape="round">View</IonButton>
                     </Link>
                  </IonCardHeader>
                </IonCard>
             
            );
          })
          ) : (
            <h3>Loading...</h3>
          )
        }
            </IonCol>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
}
  
};
  


export default Home;
